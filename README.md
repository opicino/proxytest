
just run node target.js in a terminal

and proxy.js in another terminal

then from a browser access 
```
http://0.0.0.0:8081/proxy
```
NOTE that this won't work with 

``
return await h.proxy({ host: 'digitiamo-iknowu.herokuapp.com/', port: 443, protocol: 'https', passThrough:true });
``

or 

``
return await h.proxy({ host: 'digitiamo-iknowu.herokuapp.com/', port: 80, protocol: 'http', passThrough:true });
```
