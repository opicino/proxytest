/*
  Makemehapi Workshop Exercise 6 Solution
  Based on the official solution, but modified a little bit, and changed to work with Cloud9 environemnt.
  Official solution:
  https://github.com/hapijs/makemehapi/blob/master/exercises/proxies/solution/solution.js
  License of the official solution: https://github.com/hapijs/makemehapi/blob/master/LICENSE
*/

// you should run this with
// node makemehapi-exercise-6-forwarder.js
// This is the proxy server, which goes to the another server to request the resource.

var Hapi = require('@hapi/hapi');
var H2o2 = require('@hapi/h2o2');

const init = async () => {
    var server = Hapi.Server({
        host: "0.0.0.0",
        port: 8081
    });

    await server.register(H2o2);

    server.route({
        path: '/proxy',
        method: 'GET',
        handler: async (request, h) => {
            // return await h.proxy({ host: 'digitiamo-iknowu.herokuapp.com/', port: 443, protocol: 'https', passThrough:true });
            return await h.proxy({ host: '0.0.0.0', port: 8082, protocol: 'http', passThrough:true });
        }

    });

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

init();
