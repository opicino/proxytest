/*
  Makemehapi Workshop Exercise 6 Solution
  Based on the official solution, but modified a little bit, and changed to work with Cloud9 environemnt.
  Official solution:
  https://github.com/hapijs/makemehapi/blob/master/exercises/proxies/solution/solution.js
  License of the official solution: https://github.com/hapijs/makemehapi/blob/master/LICENSE
*/

// you should run this with
// node makemehapi-exercise-6-target.js 65535
// This is the target server which is accessed by the proxy server.

var Hapi = require('@hapi/hapi');
var Inert = require('@hapi/inert');
var Path = require('path');

const init = async () => {
    var server = Hapi.Server({
        host: "0.0.0.0",
        port: 8082,
        routes: {
            files: {
                relativeTo: __dirname
            }
        }
    });

    await server.register(Inert);

    server.route({
        path: '/proxy',
        method: 'GET',
        handler: {
            file: "index.html"
        }
    });

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

init();
